package com.example.katarinfo.view.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.katarinfo.view.ui.QatarCountriesFragment
import com.example.katarinfo.view.ui.QatarFactsFragment
import com.example.katarinfo.view.ui.QatarInfoFragment

class MainPagerAdapter(fragmentActivity: FragmentActivity): FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int {
        return 3
    }

    override fun createFragment(position: Int): Fragment {
        return when(position) {
            0 -> QatarInfoFragment()
            1 -> QatarCountriesFragment()
            else -> QatarFactsFragment()
        }
    }
}