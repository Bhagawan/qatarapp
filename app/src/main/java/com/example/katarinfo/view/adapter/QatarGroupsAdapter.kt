package com.example.katarinfo.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.katarinfo.R
import com.example.katarinfo.data.QatarGroup
import com.example.katarinfo.databinding.ItemQatarGroupBinding
import com.example.katarinfo.view.viewmodel.QatarSingleGroupViewModel

class QatarGroupsAdapter: RecyclerView.Adapter<QatarGroupsAdapter.ViewHolder>(), QatarBindableAdapter<List<QatarGroup>> {
    private var groups = emptyList<QatarGroup>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemQatarGroupBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_qatar_group, parent, false)
        binding.recyclerItemQatarGroup.layoutManager = LinearLayoutManager(parent.context)
        binding.recyclerItemQatarGroup.adapter = QatarSingleGroupAdapter()
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(groups[position])
    }

    override fun getItemCount(): Int {
        return groups.size
    }

    override fun setData(data: List<QatarGroup>) {
        groups = data
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemQatarGroupBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(group: QatarGroup) {
            binding.setVariable(BR.viewModel, QatarSingleGroupViewModel(group))
        }
    }
}