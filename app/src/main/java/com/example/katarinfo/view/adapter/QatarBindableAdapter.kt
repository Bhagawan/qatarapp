package com.example.katarinfo.view.adapter

interface QatarBindableAdapter<T> {
    fun setData(data: T)
}