package com.example.katarinfo.view.viewmodel

import com.example.katarinfo.data.QatarCountry

class QatarCountryViewModel(country: QatarCountry) {

    val name = country.country

    val flagUrl = country.flag
}