package com.example.katarinfo.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.example.katarinfo.R
import com.example.katarinfo.data.QatarCountry
import com.example.katarinfo.data.QatarGroup
import com.example.katarinfo.view.viewmodel.QatarCountryViewModel
import com.example.katarinfo.view.viewmodel.QatarSingleGroupViewModel

class QatarSingleGroupAdapter: RecyclerView.Adapter<QatarSingleGroupAdapter.ViewHolder>(), QatarBindableAdapter<List<QatarCountry>> {
    private var countries = emptyList<QatarCountry>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ViewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_qatar_group_country, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(countries[position])
    }

    override fun getItemCount(): Int {
        return countries.size
    }

    override fun setData(data: List<QatarCountry>) {
        countries = data
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(country: QatarCountry) {
            binding.setVariable(BR.viewModel, QatarCountryViewModel(country))
        }
    }
}