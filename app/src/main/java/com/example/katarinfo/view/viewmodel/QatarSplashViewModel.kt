package com.example.katarinfo.view.viewmodel

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import android.view.View
import android.webkit.WebView
import android.widget.ImageView
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.BindingAdapter
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import androidx.databinding.library.baseAdapters.BR
import com.example.katarinfo.data.QatarSplashResponse
import com.example.katarinfo.util.QatarServerClient
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import im.delight.android.webview.AdvancedWebView
import io.reactivex.subjects.PublishSubject
import java.util.*

@BindingAdapter("url")
fun setUrl(v: AdvancedWebView, url: String) {
    v.loadUrl(url)
}

class QatarSplashViewModel(simLanguage: String): BaseObservable() {
    private var requestInProcess = false

    var switchToMain: PublishSubject<Boolean> = PublishSubject.create()

    @Bindable
    var url  = ""
    @Bindable
    var logoUrl = "http://195.201.125.8/QatarApp/logo.png"
    @Bindable
    var webViewVisibility = View.VISIBLE
    @Bindable
    var logoVisibility = View.GONE

    init {
        showLogo()
        QatarServerClient.create().getSplash(Locale.getDefault().language, simLanguage, Build.MODEL, TimeZone.getDefault().displayName.replace("GMT", ""))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<QatarSplashResponse> {
                lateinit var d: Disposable
                override fun onNext(t: QatarSplashResponse) {
                    if (requestInProcess) {
                        requestInProcess = false
                        hideLogo()
                    }
                    if(t.url != "no") {
                        url = "https://${t.url}"
                        notifyPropertyChanged(BR.url)
                    } else switchToMain.onNext(true)
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    switchToMain.onNext(true)
                    d.dispose()
                }

                override fun onComplete() {
                    d.dispose()
                }

                override fun onSubscribe(d: Disposable) {
                    this.d = d
                }
            })
    }

    private fun showLogo() {
        if(requestInProcess) {
            webViewVisibility = View.GONE
            notifyPropertyChanged(BR.webViewVisibility)
            logoVisibility = View.VISIBLE
            notifyPropertyChanged(BR.logoVisibility)
        }
    }

    fun hideLogo() {
        webViewVisibility = View.VISIBLE
        notifyPropertyChanged(BR.webViewVisibility)
        logoVisibility = View.GONE
        notifyPropertyChanged(BR.logoVisibility)
    }
}