package com.example.katarinfo.view.viewmodel

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.katarinfo.BR
import com.example.katarinfo.data.QatarGroup
import com.example.katarinfo.util.QatarServerClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class QatarGroupsViewModel: BaseObservable() {

    @Bindable
    var groups: List<QatarGroup> = emptyList()

    init {
        QatarServerClient.create().getQatarGroups()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                groups = it
                notifyPropertyChanged(BR.groups)
            }
    }
}