package com.example.katarinfo.view.viewmodel

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.katarinfo.BR
import com.example.katarinfo.data.QatarFact
import com.example.katarinfo.util.QatarServerClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class QatarFactsViewModel : BaseObservable() {

    @Bindable
    var facts: List<QatarFact> = emptyList()

    init {
        QatarServerClient.create().getQatarFacts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                facts = it
                notifyPropertyChanged(BR.facts)
            }
    }
}