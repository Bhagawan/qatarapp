package com.example.katarinfo.view.viewmodel

import com.example.katarinfo.data.QatarGroup

class QatarSingleGroupViewModel(group: QatarGroup) {
    val name = group.name
    val countries = group.countries
}