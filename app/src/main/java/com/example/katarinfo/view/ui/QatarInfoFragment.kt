package com.example.katarinfo.view.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.katarinfo.R
import com.example.katarinfo.databinding.FragmentQatarInfoBinding
import com.example.katarinfo.view.adapter.QatarInfoAdapter
import com.example.katarinfo.view.viewmodel.QatarInfoViewModel

class QatarInfoFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val binding = FragmentQatarInfoBinding.inflate(layoutInflater)
        binding.viewModel = QatarInfoViewModel()
        binding.recyclerInfo.layoutManager = LinearLayoutManager(context)
        binding.recyclerInfo.adapter = QatarInfoAdapter()
        return binding.root
    }

}