package com.example.katarinfo.view.adapter

import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.katarinfo.data.QatarInfoElement
import com.squareup.picasso.Picasso

class QatarInfoAdapter : RecyclerView.Adapter<QatarInfoAdapter.ViewHolder>(), QatarBindableAdapter<List<QatarInfoElement>> {
    private var info = emptyList<QatarInfoElement>()

    companion object {
        const val HEADER = 0
        const val IMAGE = 1
        const val TEXT = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when(viewType) {
            HEADER -> {
                val view = TextView(parent.context)
                view.textSize = 26.0f
                view.setTextColor(Color.BLACK)
                view.textAlignment = View.TEXT_ALIGNMENT_CENTER
                view.layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
                ViewHolder(view)
            }
            IMAGE -> {
                val view = ImageView(parent.context)
                view.layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
                ViewHolder(view)
            }
            else -> {
                val view = TextView(parent.context)
                view.textSize = 14.0f
                view.setTextColor(Color.BLACK)
                view.textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                view.layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
                ViewHolder(view)
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when(holder.itemViewType) {
            IMAGE -> Picasso.get().load(info[position].body).into(holder.view as ImageView)
            else -> (holder.view as TextView).text = info[position].body
        }
    }

    override fun getItemCount(): Int {
        return info.size
    }

    override fun setData(data: List<QatarInfoElement>) {
        info = data
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return when(info[position].type) {
            "header" -> HEADER
            "image" -> IMAGE
            else -> TEXT
        }
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}