package com.example.katarinfo.view.viewmodel

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.katarinfo.BR
import com.example.katarinfo.data.QatarInfoElement
import com.example.katarinfo.util.QatarServerClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class QatarInfoViewModel : BaseObservable() {

    @Bindable
    var info: List<QatarInfoElement> = emptyList()

    init {
        QatarServerClient.create().getQatarInfo()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                info = it
                notifyPropertyChanged(BR.info)
            }
    }
}