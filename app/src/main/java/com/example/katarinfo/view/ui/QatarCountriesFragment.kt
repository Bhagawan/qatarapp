package com.example.katarinfo.view.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.katarinfo.R
import com.example.katarinfo.databinding.FragmentCountriesBinding
import com.example.katarinfo.databinding.FragmentQatarInfoBinding
import com.example.katarinfo.view.adapter.QatarGroupsAdapter
import com.example.katarinfo.view.adapter.QatarInfoAdapter
import com.example.katarinfo.view.viewmodel.QatarGroupsViewModel
import com.example.katarinfo.view.viewmodel.QatarInfoViewModel

class QatarCountriesFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val binding = FragmentCountriesBinding.inflate(layoutInflater)
        binding.viewModel = QatarGroupsViewModel()
        binding.recyclerGroups.layoutManager = LinearLayoutManager(context)
        binding.recyclerGroups.adapter = QatarGroupsAdapter()
        return binding.root
    }

}