package com.example.katarinfo.view.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.katarinfo.R
import com.example.katarinfo.databinding.FragmentQatarFactsBinding
import com.example.katarinfo.databinding.FragmentQatarInfoBinding
import com.example.katarinfo.view.adapter.QatarFactsAdapter
import com.example.katarinfo.view.adapter.QatarInfoAdapter
import com.example.katarinfo.view.viewmodel.QatarFactsViewModel
import com.example.katarinfo.view.viewmodel.QatarInfoViewModel

class QatarFactsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val binding = FragmentQatarFactsBinding.inflate(layoutInflater)
        binding.viewModel = QatarFactsViewModel()
        binding.recyclerFacts.layoutManager = LinearLayoutManager(context)
        binding.recyclerFacts.adapter = QatarFactsAdapter()
        return binding.root
    }

}