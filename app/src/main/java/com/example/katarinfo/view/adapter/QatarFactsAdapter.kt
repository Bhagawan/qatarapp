package com.example.katarinfo.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.example.katarinfo.R
import com.example.katarinfo.data.QatarFact
import com.example.katarinfo.view.viewmodel.QatarSingleFactViewModel

class QatarFactsAdapter: RecyclerView.Adapter<QatarFactsAdapter.ViewHolder>(), QatarBindableAdapter<List<QatarFact>> {
    private var facts = emptyList<QatarFact>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ViewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_qatar_fact, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(facts[position])
    }

    override fun getItemCount(): Int {
        return facts.size
    }

    override fun setData(data: List<QatarFact>) {
        facts = data
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(fact: QatarFact) {
            binding.setVariable(BR.viewModel, QatarSingleFactViewModel(fact))
        }
    }
}