package com.example.katarinfo.data

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class QatarCountry(@SerializedName("country") val country: String, @SerializedName("flag") val flag: String)
