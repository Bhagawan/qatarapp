package com.example.katarinfo.data

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class QatarInfoElement(@SerializedName("type") val type: String, @SerializedName("body") val body: String)

