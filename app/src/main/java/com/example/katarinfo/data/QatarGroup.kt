package com.example.katarinfo.data

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class QatarGroup(@SerializedName("name") val name: String, @SerializedName("countries") val countries: List<QatarCountry>)
