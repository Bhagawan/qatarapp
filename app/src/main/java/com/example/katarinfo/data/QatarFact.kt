package com.example.katarinfo.data

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class QatarFact(@SerializedName("image") val image: String, @SerializedName("text") val text: String)
