package com.example.katarinfo.data

import androidx.annotation.Keep

@Keep
data class QatarSplashResponse(val url : String)

