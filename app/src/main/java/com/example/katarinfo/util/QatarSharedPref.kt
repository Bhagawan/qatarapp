package com.example.katarinfo.util

import android.content.Context
import java.util.*

class QatarSharedPref {
    companion object {
        fun checkId(context: Context) {
            val shP = context.getSharedPreferences("Qatar", Context.MODE_PRIVATE)
            if(!shP.contains("id")) shP.edit().putString("id", UUID.randomUUID().toString()).apply()
        }
        
        fun getId(context: Context): String {
            val shP = context.getSharedPreferences("Qatar", Context.MODE_PRIVATE)
            if(!shP.contains("id")) shP.edit().putString("id", UUID.randomUUID().toString()).apply()
            return shP.getString("key", "") ?: ""
        }
    }
}