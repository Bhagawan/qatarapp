package com.example.katarinfo.util

import com.example.katarinfo.data.QatarFact
import com.example.katarinfo.data.QatarGroup
import com.example.katarinfo.data.QatarInfoElement
import com.example.katarinfo.data.QatarSplashResponse
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface QatarServerClient {

    @FormUrlEncoded
    @POST("QatarApp/splash.php")
    fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String): Observable<QatarSplashResponse>

    @GET("QatarApp/info.json")
    fun getQatarInfo(): Observable<List<QatarInfoElement>>

    @GET("QatarApp/groups.json")
    fun getQatarGroups(): Observable<List<QatarGroup>>

    @GET("QatarApp/facts.json")
    fun getQatarFacts(): Observable<List<QatarFact>>

    companion object {
        fun create() : QatarServerClient {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.newThread()))
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(QatarServerClient::class.java)
        }
    }
}