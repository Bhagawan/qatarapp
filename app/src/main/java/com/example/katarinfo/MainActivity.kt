package com.example.katarinfo

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.katarinfo.util.QatarSharedPref
import com.example.katarinfo.view.adapter.QatarBindableAdapter
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.lang.Exception

@Suppress("UNCHECKED_CAST")
@BindingAdapter("recyclerData")
fun <T> setRecyclerData(v: RecyclerView, data: T) {
    if(v.adapter != null){
        if(v.adapter is QatarBindableAdapter<*>) {
            (v.adapter as QatarBindableAdapter<T>).setData(data)
        }
    }
}

@BindingAdapter("imageData")
fun setImageFromUrl(v: ImageView, url: String) {
    Picasso.get().load(url).into(v)
}

class MainActivity : AppCompatActivity() {
    private lateinit var target: Target

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<BottomNavigationView>(R.id.bottomNavView).setupWithNavController(findNavController(R.id.fragmentContainerView))
        loadQatarBackground()
        QatarSharedPref.checkId(this)
    }

    private fun loadQatarBackground() {
        target = object: Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                bitmap?.let {
                    findViewById<ConstraintLayout>(R.id.layout_main).background = BitmapDrawable(resources, it)
                }
            }

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        Picasso.get().load("http://195.201.125.8/QatarApp/back.png").into(target)
    }
}