package com.example.katarinfo

import android.app.Application
import com.appsflyer.AppsFlyerLib
import com.onesignal.OneSignal

const val APPSFLYER_ID = "1"
const val ONESIGNAL_APP_ID = "0f7a1f96-50aa-4b15-83a9-5baa1014d5d4"

class QatarApp: Application() {

    override fun onCreate() {
        super.onCreate()
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE)
        OneSignal.initWithContext(this)
        OneSignal.setAppId(ONESIGNAL_APP_ID)
        AppsFlyerLib.getInstance().init(APPSFLYER_ID, null, this)
        AppsFlyerLib.getInstance().start(this)
    }
}